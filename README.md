# Freskein - Freshly washed folder

## Description

Freskein is the concatenation of the two brittany word fresk and kein.

Freskein removes execution rights on files that do not have to be
executable by default, like images, office documents...
Too many users or programs do not remove these rights on the files they create.
Freskein manages all files located in the current folder in which it is launched.

Note that all files with a backslash (\) in their name will automatically
have no execution rights, even if it turns out to be a legal program or script.
It is too dangerous for the computer security to have files
with this kind of characters in their name.

All the command help and description is on the online manual
[freskein](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/freskein/).

## Debian package

Debian is a GNU/Linux distribution.
Debian (and certainly Ubuntu) package for all arch could be download on: https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/freskein/download.

You can then install it with

```bash
sudo dpkg -i freskein_*_all.deb
```
(just replace * with the version you have donwloaded).

## Repository / Contribute

### Source

The whole code is under **free license**.
The script in ```bash``` is under GPL version 2 or more recent (http://www.gnu.org/licenses/gpl.html).
All the source code is available on the forge of the Grenoble campus:
https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/freskein
The sources are managed via Git (GitLab).
It is very easy to stay synchronized with these sources.

* The initial recovery
  ```bash
  git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/freskein
  ```
* The updates afterwards
  ```bash
  git pull
  ```
* Contribute.
  It is possible to contribute by proposing pull requests,
  merge requests or simply old fashioned patches.

### Patch

It is possible to have a writing access to the project on the forge
on motivated request to [Gabriel Moreau](mailto:Gabriel.Moreau_A_legi.grenoble-inp.fr).
For questions of administration time and security,
the project is not directly accessible in writing without authorization.
For questions of decentralization of the web, of autonomy
and non-allegiance to the ambient (and North American) centralism,
we use the forge of the university campus of Grenoble...

You can propose a patch by email of a particular file via the ```diff``` command:
```bash
diff -u freskein.org freskein.new > freskein.patch
```
The patch is applied (after reading and rereading it) via the command:
```bash
patch -p0 < freskein.patch
```

## COPYRIGHT

Copyright (C) 2019-2022, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France

This project was originally written by
Gabriel Moreau <Gabriel.Moreau@univ-grenoble-alpes.fr>.

Licence: [GNU GPL version 2 or later](https://spdx.org/licenses/GPL-2.0-or-later.html)
