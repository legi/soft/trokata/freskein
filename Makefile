
SHELL:=/bin/bash


.PHONY: all test clean man pkg pages

all: pkg

test:
	touch test\ .txt test\'.txt test\".txt test\*.txt test\?.txt 'test\$HOME.txt' 'test\n.txt' test\&.txt test\\.txt test\\\*.txt
	chmod a+rx test*
	./freskein
	ls -al test*

clean:
	@rm -f test* *.1.gz *.html *.tmp
	@rm -rf public

man:
	@pod2man freskein | gzip > freskein.1.gz
	@pod2html --css podstyle.css --index --header freskein > freskein.html

pkg: man
	./make-package-debian

pages: pkg
	mkdir -p public/download
	cp -p *.html       public/
	cp -p podstyle.css public/
	cp -p LICENSE.md   public/
	cp -p --no-clobber freskein_*_all.deb  public/download/
	cd public; ln -sf freskein.html index.html
	echo '<html><body><h1>Freskein Debian Package</h1><ul>' > public/download/index.html
	(cd public/download; while read file; do printf '<li><a href="%s">%s</a> (%s)</li>\n' $$file $$file $$(stat -c %y $$file | cut -f 1 -d ' '); done < <(ls -1t *.deb) >> index.html)
	echo '</ul></body></html>' >> public/download/index.html
