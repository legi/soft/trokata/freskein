#!/bin/bash
#
# 2019/12/19 Gabriel Moreau <Gabriel.Moreau@univ-grenoble-alpes.fr>

export PATH=/bin:/sbin:/usr/bin:/usr/sbin
export LANG=C

export VERSION=0.6.3

set -e

# file with \ in the name
find . -name '*\\*' -exec chmod a-x {} \+

find . -type f -a -perm /0100 -exec file --print0 {} \+ \
   | egrep -v -a --null '(executable|ELF .* LSB shared object, .* dynamically linked, interpreter /lib)' \
   | cut -f 1 -d '' --output-delimiter='\n' \
   | while read filename
      do
         [ -f "${filename}" ] && chmod a-x "${filename}"
      done
exit


################################################################
# documentation
################################################################

__END__

=head1 NAME

freskein - freshly washed folder

=head1 SYNOPSIS

 freskein

=head1 DESCRIPTION

Freskein is the concatenation of the two brittany word fresk and kein.

Freskein removes execution rights on files that do not have to be
executable by default, like images, office documents...
Too many users or programs do not remove these rights on the files they create.
Freskein manages all files located in the current folder in which it is launched.

Note that all files with a backslash (\) in their name will automatically
have no execution rights, even if it turns out to be a legal program or script.
It is too dangerous for the computer security to have files
with this kind of characters in their name.

=head1 AUTHORS

Written by Gabriel Moreau <Gabriel.Moreau@univ-grenoble-alpes.fr>, Grenoble - France


=head1 COPYRIGHT

Copyright (C) 2019-2022, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France

Licence: GNU GPL version 2 or later
